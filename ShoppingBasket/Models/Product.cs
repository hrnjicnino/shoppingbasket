﻿using ShoppingBasket.Models.Contracts;

namespace ShoppingBasket.Models
{
    public class Product : IItem
    {
        public Product(string id, string name, decimal price, int quantity = 1)
        {
            Id = id;
            Name = name;
            Price = price;
            Quantity = quantity;
        }

        public string Id { get; }
        public string Name { get; }
        public decimal Price { get; }
        public int Quantity { get; set; }
    }
}
