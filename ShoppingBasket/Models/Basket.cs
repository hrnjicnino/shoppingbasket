﻿using ShoppingBasket.Models.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace ShoppingBasket.Models
{
    internal class Basket : IBasket
    {
        readonly Dictionary<string, IItem> basket;

        public Basket()
        {
            basket = new Dictionary<string, IItem>();
        }

        public List<IItem> GetItems()
        {
            return basket.Values.ToList();
        }

        public IItem GetItem(string itemId)
        {
            if (basket.ContainsKey(itemId))
            {
                return basket[itemId];
            }
            else
            {
                return null;
            }
        }

        public void AddItem(IItem item)
        {
            if (basket.ContainsKey(item.Id))
            {
                basket[item.Id].Quantity = item.Quantity;
            }
            else
            {
                basket.Add(item.Id, item);
            }
        }
    }
}
