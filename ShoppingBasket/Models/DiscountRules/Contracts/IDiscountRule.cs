﻿using ShoppingBasket.Models.Contracts;

namespace ShoppingBasket.Models.DiscountRules.Contracts
{
    public interface IDiscountRule
    {
        /// <summary>
        /// Determines if the discount is applicable for the basket based on its contents
        /// </summary>
        bool IsValid(IBasket basket);
    }
}
