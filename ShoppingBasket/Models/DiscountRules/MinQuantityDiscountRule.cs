﻿using ShoppingBasket.Models.Contracts;
using ShoppingBasket.Models.DiscountRules.Contracts;

namespace ShoppingBasket.Models.DiscountRules
{
    public class MinQuantityDiscountRule : IDiscountRule
    {
        public MinQuantityDiscountRule(string forItemId, int minQuantity)
        {
            ForItemId = forItemId;
            MinQuantity = minQuantity;
        }

        string ForItemId { get; }

        int MinQuantity { get; }

        public bool IsValid(IBasket basket)
        {
            var basketItem = basket.GetItem(ForItemId);

            if (basketItem == null || basketItem.Quantity < MinQuantity)
                return false;

            return true;
        }
    }
}
