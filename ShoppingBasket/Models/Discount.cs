﻿
using ShoppingBasket.Models.Contracts;
using ShoppingBasket.Models.DiscountRules.Contracts;
using ShoppingBasket.Models.DiscountTypes.Contracts;
using System.Collections.Generic;

namespace ShoppingBasket.Models
{
    public class Discount : IDiscount
    {
        public string ForItemId { get; }

        public IDiscountType DiscountType { get; }

        public List<IDiscountRule> Rules { get; }

        public Discount(string forItemId, IDiscountType discountType)
        {
            ForItemId = forItemId;
            DiscountType = discountType;
            Rules = new List<IDiscountRule>();
        }
    }
}
