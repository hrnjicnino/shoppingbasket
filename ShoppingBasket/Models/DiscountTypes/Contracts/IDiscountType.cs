﻿using ShoppingBasket.Models.Contracts;

namespace ShoppingBasket.Models.DiscountTypes.Contracts
{
    public interface IDiscountType
    {
        /// <summary>
        /// Determines the discount calculation for a given item id if the item is found in the basket
        /// </summary>
        decimal Calculate(IBasket basket, string itemId);
    }
}
