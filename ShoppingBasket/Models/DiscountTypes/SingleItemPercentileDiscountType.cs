﻿using ShoppingBasket.Models.Contracts;
using ShoppingBasket.Models.DiscountTypes.Contracts;

namespace ShoppingBasket.Models.DiscountTypes
{
    public class SingleItemPercentileDiscountType : IDiscountType
    {
        public SingleItemPercentileDiscountType(int percent)
        {
            Percent = percent;

        }
        public int Percent { get;}

        public decimal Calculate(IBasket basket, string itemId)
        {
            var basketItem = basket.GetItem(itemId);

            if (basketItem == null)
                return 0M;

            // Discount is calculated for single item
            return basketItem.Price * Percent / 100;       
        }
    }
}
