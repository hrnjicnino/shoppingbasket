﻿using ShoppingBasket.Models.Contracts;
using ShoppingBasket.Models.DiscountTypes.Contracts;

namespace ShoppingBasket.Models.DiscountTypes
{
    public class EveryNthItemDiscountType : IDiscountType
    {
        
        public EveryNthItemDiscountType(int everyNthItem)
        {
            EveryNthItem = everyNthItem;
        }

        public int EveryNthItem { get; }

        public decimal Calculate(IBasket basket, string itemId)
        {

            var basketItem = basket.GetItem(itemId);

            if (basketItem == null)
                return 0M;

            // Discount is equal to the sum of the free items
            return basketItem.Quantity / EveryNthItem * basketItem.Price;          
        }
    }
}
