﻿using System.Collections.Generic;

namespace ShoppingBasket.Models.Contracts
{
    public interface IBasket
    {
        List<IItem> GetItems();

        IItem GetItem(string itemId);

        void AddItem(IItem item);
    }
}
