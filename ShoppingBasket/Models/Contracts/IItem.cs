﻿namespace ShoppingBasket.Models.Contracts
{
    public interface IItem
    {
        string Id { get; }

        string Name { get; }

        decimal Price { get; }

        int Quantity { get; set; }
    }
}
