﻿using ShoppingBasket.Models.DiscountRules.Contracts;
using ShoppingBasket.Models.DiscountTypes.Contracts;
using System.Collections.Generic;

namespace ShoppingBasket.Models.Contracts
{
    public interface IDiscount
    {
        string ForItemId { get; }

        IDiscountType DiscountType { get; }

        List<IDiscountRule> Rules { get; }
    }
}
