﻿using ShoppingBasket.Models.Contracts;
using System.Collections.Generic;

namespace ShoppingBasket.Services.Contracts
{
    public interface ICalculatorService
    {
        decimal CalculateTotal(IBasket basket, List<IDiscount> discounts);
    }
}
