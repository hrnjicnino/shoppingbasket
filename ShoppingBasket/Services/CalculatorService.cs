﻿using ShoppingBasket.Models.Contracts;
using ShoppingBasket.Services.Contracts;
using System.Collections.Generic;

namespace ShoppingBasket.Services
{
    internal class CalculatorService : ICalculatorService
    {
        public decimal CalculateTotal(IBasket basket, List<IDiscount> discounts)
        {
            decimal totalWithoutDiscounts = 0;
            decimal totalDiscount = 0;

            var items = basket.GetItems();
            foreach (var item in items)
            {
                var itemSum = item.Price * item.Quantity;
                totalWithoutDiscounts += itemSum;

                foreach (var discount in discounts)
                {
                    var itemDiscount = CalculateDiscountForBasketItem(discount, basket, item);
                    totalDiscount += itemDiscount;
                }
            }

            return totalWithoutDiscounts - totalDiscount;
        }

        decimal CalculateDiscountForBasketItem(IDiscount discount, IBasket basket, IItem item)
        {
            // No discount
            if (!discount.ForItemId.Equals(item.Id))
                return 0;

            // Check all discount rules to see if discount is applicable
            var isValid = true;
            foreach (var rule in discount.Rules)
            {
                if (!rule.IsValid(basket))
                {
                    isValid = false;
                    break;
                }
            }

            // No discount
            if (!isValid)
                return 0;

            // Calculate discount
            return discount.DiscountType.Calculate(basket, item.Id);
        }
    }
}
