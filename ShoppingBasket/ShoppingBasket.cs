﻿using ShoppingBasket.Models;
using ShoppingBasket.Models.Contracts;
using ShoppingBasket.Services;
using ShoppingBasket.Services.Contracts;
using System.Collections.Generic;

namespace ShoppingBasket
{
    public class ShoppingBasket : IShoppingBasket
    {
        readonly IBasket basket;
        readonly ICalculatorService calculator;
        readonly List<IDiscount> discounts;

        public ShoppingBasket(List<IDiscount> discounts)
        {
            // Shopping basket composition
            basket = new Basket();
            calculator = new CalculatorService();
            this.discounts = discounts;
        }

        public void AddProducts(IItem item)
        {
            basket.AddItem(item);
        }

        public decimal CalculateTotal()
        {
            return calculator.CalculateTotal(basket, discounts);
        }
    }
}
