﻿using ShoppingBasket.Models.Contracts;

namespace ShoppingBasket
{
    interface IShoppingBasket
    {
        void AddProducts(IItem item);

        decimal CalculateTotal();
    }
}
