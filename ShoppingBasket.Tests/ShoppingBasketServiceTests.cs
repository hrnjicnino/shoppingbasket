using ShoppingBasket.Models;
using ShoppingBasket.Models.Contracts;
using ShoppingBasket.Models.DiscountRules;
using ShoppingBasket.Models.DiscountTypes;
using System.Collections.Generic;
using Xunit;


namespace ShoppingBasket.Tests
{
    /// <summary>
    /// Testing calculation of the total cost of products:
    /// Product | Price
    /// ---------------
    /// Butter  | $0.80
    /// Milk    | $1.15
    /// Bread   | $1.00
    /// with active discounts:
    /// 1. Buy 2 butters and get one bread at 50% off
    /// 2. Buy 3 milks and get 4th milk for free
    /// </summary>

    public class ShoppingBasketServiceTests
    {
        public static class WarehouseServiceMock
        {
            // Mock products used in the test for the product ids consistency and ease of access
            public readonly static Product Bread = new Product("1", "Bread", 1M);
            public readonly static Product Butter = new Product("2", "Butter", 0.8M);
            public readonly static Product Milk = new Product("3", "Milk", 1.15M);

            // Composed discounts as per requirement
            public static List<IDiscount> GetDiscounts()
            {
                var discounts = new List<IDiscount>();

                // Buy 2 butters and get one bread at 50% off
                var discountType50Percent = new SingleItemPercentileDiscountType(50);
                var discountRuleMinButterQuantity2 = new MinQuantityDiscountRule(Butter.Id, 2);
                var breadDiscount = new Discount(Bread.Id, discountType50Percent);
                breadDiscount.Rules.Add(discountRuleMinButterQuantity2);
                discounts.Add(breadDiscount);

                // Buy 3 milks and get 4th milk for free
                var discountType4thItem = new EveryNthItemDiscountType(4);
                var milkDiscount = new Discount(Milk.Id, discountType4thItem);
                discounts.Add(milkDiscount);

                return discounts;
            }
        }

        public class CalculateTotalTest : ShoppingBasketServiceTests
        {
            // Used for the organization of the test scenarios
            public class TestInputDataWithExpectedOutput
            {
                public TestInputDataWithExpectedOutput(IItem[] input, decimal output)
                {
                    Input = input;
                    Output = output;
                }
                public IItem[] Input { get; }
                public decimal Output { get; }
            }

            // The test data organized by scenarios
            public static TheoryData<TestInputDataWithExpectedOutput> Data => new TheoryData<TestInputDataWithExpectedOutput>()
            {
                // Scenario 1 - The basket has 1 bread, 1 butter, 1 milk => result should be 2.95
                new TestInputDataWithExpectedOutput( new Product[]
                    {
                        new Product(WarehouseServiceMock.Butter.Id, WarehouseServiceMock.Butter.Name, WarehouseServiceMock.Butter.Price, 1),
                        new Product(WarehouseServiceMock.Milk.Id, WarehouseServiceMock.Milk.Name, WarehouseServiceMock.Milk.Price, 1),
                        new Product(WarehouseServiceMock.Bread.Id, WarehouseServiceMock.Bread.Name, WarehouseServiceMock.Bread.Price, 1)
                    },
                    2.95M),

                // Scenario 2 - The basket has 2 breads, 2 butters => result should be 3.10
                new TestInputDataWithExpectedOutput( new Product[]
                    {
                        new Product(WarehouseServiceMock.Butter.Id, WarehouseServiceMock.Butter.Name, WarehouseServiceMock.Butter.Price, 2),
                        new Product(WarehouseServiceMock.Bread.Id, WarehouseServiceMock.Bread.Name, WarehouseServiceMock.Bread.Price, 2)
                    },
                    3.1M),

                // Scenario 3 - The basket has 4 milks => result should be 3.45
                new TestInputDataWithExpectedOutput( new Product[]
                    {
                        new Product(WarehouseServiceMock.Milk.Id, WarehouseServiceMock.Milk.Name, WarehouseServiceMock.Milk.Price, 4)
                    },
                    3.45M),

                //Scenario 4 - The basket has 1 bread, 2 butters, 8 milks => result should be 9.00
                new TestInputDataWithExpectedOutput( new Product[]
                    {
                        new Product(WarehouseServiceMock.Butter.Id, WarehouseServiceMock.Butter.Name, WarehouseServiceMock.Butter.Price, 2),
                        new Product(WarehouseServiceMock.Milk.Id, WarehouseServiceMock.Milk.Name, WarehouseServiceMock.Milk.Price, 8),
                        new Product(WarehouseServiceMock.Bread.Id, WarehouseServiceMock.Bread.Name, WarehouseServiceMock.Bread.Price, 1)
                    },
                    9M)
            };

            [Theory]
            [MemberData(nameof(Data))]
            public void CalculateTotal(TestInputDataWithExpectedOutput data)
            {
                // Arrange
                var discounts = WarehouseServiceMock.GetDiscounts();
                var shoppingBasket = new ShoppingBasket(discounts);

                // add items
                foreach (var item in data.Input)
                {
                    shoppingBasket.AddProducts(item);
                }

                // Act
                var result = shoppingBasket.CalculateTotal();

                // Assert
                Assert.Equal(data.Output, result);
            }
        }
    }
}
